+++
[[resources]]
	src = "1.png"
	name = "Crush kita"
	[resources.params]
		order = "1"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/crushkita"

[[resources]]
	src = "2.png"
	name = "Woof!"
	[resources.params]
		order = "2"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/woof"

[[resources]]
	src = "3.png"
	name = "Sticker Signature"
	[resources.params]
		order = "3"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/stickersig"

[[resources]]
	src = "4.png"
	name = "Thonking"
	[resources.params]
		order = "4"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/thonkemoji"
+++
