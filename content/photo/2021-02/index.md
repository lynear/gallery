+++
[[resources]]
	src = "1.png"
	name = "Happy Lynx"
	[resources.params]
		order = "1"
		description = "Layla being happy!"
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/happylynx"

[[resources]]
	src = "2.png"
	name = "Valentines gift"
	[resources.params]
		order = "2"
		description = "Valentines gift for Spinel"
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/valentines"

[[resources]]
	src = "3.png"
	name = "My first commission"
	[resources.params]
		order = "3"
		description = "My first ever commission from my older sister"
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/firstcommission"
+++
