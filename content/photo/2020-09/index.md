+++
[[resources]]
	src = "1.png"
	name = "Dabbing Rathian"
	[resources.params]
		order = "1"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/dabian"

[[resources]]
	src = "2.png"
	name = "Bareta ng Sabon"
	[resources.params]
		order = "2"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/baretangsabon"

[[resources]]
	src = "3.png"
	name = "Professor Zinny"
	[resources.params]
		order = "3"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/profzinny"

[[resources]]
	src = "4.png"
	name = "POG LYNX"
	[resources.params]
		order = "4"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/poggers"
+++
