+++
[[resources]]
	src = "1.png"
	name = "Don't you drift away"
	[resources.params]
		order = "1"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/sleeping"

[[resources]]
	src = "2.png"
	name = "You're pretty good!"
	[resources.params]
		order = "2"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/prettygood"

[[resources]]
	src = "3.png"
	name = ""
	[resources.params]
		order = "3"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/"

[[resources]]
	src = "4.png"
	name = ""
	[resources.params]
		order = "4"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/"
+++
