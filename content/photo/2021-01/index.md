+++
[[resources]]
	src = "1.png"
	name = "Dabbing Rathalos"
	[resources.params]
		order = "1"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/dabalos"

[[resources]]
	src = "2.png"
	name = "Nicole Watterson"
	[resources.params]
		order = "2"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/nicolewatterson"

[[resources]]
	src = "3.png"
	name = "Cipheriana"
	[resources.params]
		order = "3"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/cipheriana"
+++
