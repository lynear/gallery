+++
[[resources]]
	src = "1.png"
	name = "Briefing"
	[resources.params]
		order = "1"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/briefing"

[[resources]]
	src = "2.png"
	name = "OK hand"
	[resources.params]
		order = "2"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/okhand"
+++
