+++
[[resources]]
	src = "1.png"
	name = "Anthro Rathian"
	[resources.params]
		order = "1"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/rathianthro"

[[resources]]
	src = "2.png"
	name = "Smug"
	[resources.params]
		order = "2"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/smug"

[[resources]]
	src = "3.png"
	name = "Shattered Glass"
	[resources.params]
		order = "3"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/shatteredglass"

[[resources]]
	src = "4.png"
	name = ""
	[resources.params]
		order = "4"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/"
+++
