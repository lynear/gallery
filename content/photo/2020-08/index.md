+++
[[resources]]
	src = "1.png"
	name = "Finger gun"
	[resources.params]
		order = "1"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/fingergun"

[[resources]]
	src = "2.png"
	name = "Thumbs up"
	[resources.params]
		order = "2"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/thumbsup"

[[resources]]
	src = "3.png"
	name = "Guildmarm Brachydios"
	[resources.params]
		order = "3"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/brachymarm"

[[resources]]
	src = "4.png"
	name = "Tapsilog"
	[resources.params]
		order = "4"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/tapsilog"

[[resources]]
	src = "5.png"
	name = "Lewd Cipher"
	[resources.params]
		order = "5"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/lewdcipher"
+++
