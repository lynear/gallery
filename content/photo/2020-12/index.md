+++
date = "2021-03-01"
[[resources]]
	src = "1.png"
	name = "Angel"
	[resources.params]
		order = "1"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/angel"

[[resources]]
	src = "2.png"
	name = "Ao Lie"
	[resources.params]
		order = "2"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/aolie"

[[resources]]
	src = "3.png"
	name = "Peace Tayo!"
	[resources.params]
		order = "3"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/peacetayo"
+++
