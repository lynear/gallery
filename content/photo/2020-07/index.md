+++
[[resources]]
	src = "1.png"
	name = "Gear Rex"
	[resources.params]
		order = "1"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/gearrex"

[[resources]]
	src = "2.png"
	name = "Mugshot"
	[resources.params]
		order = "2"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/mugshot"

[[resources]]
	src = "3.png"
	name = "A Friendly Gift"
	[resources.params]
		order = "3"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/friendlygift"

[[resources]]
	src = "4.png"
	name = "Kingina ang init"
	[resources.params]
		order = "4"
		description = ""
		button_text = "Link to post"
		button_url = "https://lynear24.github.io/post/mainit"
+++
